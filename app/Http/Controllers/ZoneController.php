<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreZone;
use App\Http\Requests\UpdateZone;
use App\Models\Website;
use App\Models\Zone;
use Webpatser\Uuid\Uuid;

class ZoneController extends Controller
{

    public function index(Website $website = null )
    {

        if (!$website)
        {
            $selected_website = new Website();
            $collection = Zone::with('website')->get();
        } else {
            $selected_website = $website;
            $collection = $website->zones()->get();
        }

        return view('pages.zone.index', compact('collection' , 'selected_website' ));
    }


    public function create(Website $website)
    {
        $selected_website = $website;
        $zone = new Zone();
        return view('pages.zone.createOrUpdate' , compact('zone','selected_website'));
    }

    public function store(Website $website , StoreZone $request)
    {

        $zone = new Zone();
        $values = $request->only($zone->getFillable());
        $values['uuid'] = (string)Uuid::generate();
        $zone->fill($values);
        $zone->save();

        return redirect()->route('websites.zones.index' , [$website->id]);
    }


    public function show(Website $website , Zone $zone)
    {
        return redirect()->route('websites.zones.edit' , [$website->id , $zone->id]);
    }

    public function edit(Website $website , Zone $zone)
    {
        $selected_website = $website;
        return view('pages.zone.createOrUpdate' , compact('zone' , 'selected_website'));
    }


    public function update(UpdateZone $request,Website $website , Zone $zone)
    {
        $values = $request->only($zone->getFillable());
        if (!$zone->uuid)
            $values['uuid'] = (string)Uuid::generate();
        $zone->fill($values);
        $zone->save();

        return redirect()->route('websites.zones.index' , [$website->id]);
    }


    public function destroy($id)
    {
        //
    }


}
