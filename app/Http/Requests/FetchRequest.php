<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class FetchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'refer' => 'required',
            'zone' => 'required',
        ];
    }

    /**
     * @return array
     */
    public function validationData()
    {
        $fields = [
            'refer',
            'category',
            'role',
            'brand',
            'motor',
            'zone',
        ];
        return $this->only($fields);
    }

    protected function failedValidation(Validator $validator){
        abort(422);
    }
}
