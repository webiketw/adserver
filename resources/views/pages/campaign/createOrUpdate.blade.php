@extends('layouts.app')

@section('content')

    @php
        if ($campaign->id){
            $route = route('advertisers.campaigns.update' , [$selected_advertiser->id , $campaign->id]) ;
            $method='PUT';
            $title = 'Edit';
        } else {
            $route = route('advertisers.campaigns.store' , [$selected_advertiser->id]);
            $method='POST';
            $title = 'New';
        }
    @endphp


    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        {{ $title }} Campaign
                    </h2>

                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <form method="POST" action="{{ $route }}">
                            @csrf
                            @method($method)

                            <input type="hidden" name="advertiser_id" value="{{ $selected_advertiser->id }}"/>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="campaign-name">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="campaign-name" required
                                           name="name" value="{{ old('name') ?? $campaign->name }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="campaign-priority">Priority</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="campaign-priority" required step="0.1"
                                           name="priority" value="{{ old('priority') ?? $campaign->priority }}">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="campaign-activate_time">Activate</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" id="campaign-activate_time"
                                           required pattern="\d{4}-\d{2}-\d{2}" name="activate_time"
                                           value="{{ old('activate_time') ?? substr($campaign->activate_time , 0 , 10) }}">
                                    <span class="help-block">
                                        00:00:00
                                    </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="campaign-expire_time">Expire</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control" id="campaign-expire_time"
                                           required pattern="\d{4}-\d{2}-\d{2}" name="expire_time"
                                           value="{{ old('expire_time') ?? substr($campaign->expire_time , 0 , 10) }}">
                                    <span class="help-block">
                                        23:59:59
                                    </span>
                                </div>
                            </div>


                            <div
                                class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row-reverse">
                                <button type="submit" class="btn btn-primary ml-3">Save changes</button>
                                <a type="button" class="btn btn-secondary ml-3" href="javascript:javascript:history.go(-1)">Close</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
