@extends('layouts.app')

@push('page-styles')
    <style type="text/css">
        #panel-1 .dropdown-item{
            padding: 0.3rem 1.5rem;
        }
    </style>
@endpush

@section('content')


    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        Banner
                    </h2>

                    <div class="btn-group">
                        <button class="btn btn-secondary btn-xs dropdown-toggle" type="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Advertiser : {{ (isset($selected_advertiser) && $selected_advertiser->id) ? $selected_advertiser->name : '請選擇' }}
                        </button>
                        @if (count($advertisers->except([$selected_advertiser->id])))
                            <div class="dropdown-menu" >
                                @foreach($advertisers->except([$selected_advertiser->id]) as $advertiser)
                                    <a class="dropdown-item"
                                       href="{{ route('advertisers.banners.index' , [$advertiser->id]) }}">{{ $advertiser->name }}</a>
                                @endforeach
                            </div>
                        @endif
                    </div>


                    <div class="btn-group ml-3">
                        <button class="btn btn-secondary btn-xs dropdown-toggle" type="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Campaign : {{ (isset($selected_campaign) && $selected_campaign->id) ? $selected_campaign->name : '請選擇' }}
                        </button>
                        @if (count($selected_advertiser->campaigns->except([$selected_campaign->id])))
                            <div class="dropdown-menu" >
                                @foreach($selected_advertiser->campaigns->except([$selected_campaign->id]) as $campaign)
                                    <a class="dropdown-item"
                                       href="{{ route('advertisers.campaigns.banners.index' , [$selected_advertiser->id,$campaign->id]) }}">{{ $campaign->name }}</a>
                                @endforeach
                            </div>
                        @endif
                    </div>


                    <div class="panel-toolbar">

                        @if (isset($selected_campaign) && $selected_campaign->id)
                            <a type="button" class="btn btn-xs btn-success ml-3"
                               href="{{ route('advertisers.campaigns.banners.create' , [$selected_advertiser->id,$selected_campaign->id]) }}">
                                Create new banner
                            </a>
                        @endif

                    </div>

                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <table class="table table-bordered table-hover table-striped">
                            <thead class="bg-fusion-50">
                            @if (!isset($selected_advertiser) || !$selected_advertiser->id)
                                <th>
                                    Advertiser
                                </th>
                            @endif
                            <th>
                                Campaign
                            </th>
                            <th>
                                Name
                            </th>

                            <th>
                                Priority
                            </th>
                            <th>
                                Website
                            </th>
                            <th>
                                Zone
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                Action
                            </th>
                            </thead>
                            <tbody>
                            @foreach ($collection as $entity)
                                <tr style="{{ $entity->status ? '' : 'color:#dfdfdf;' }}">
                                    @if (!isset($selected_advertiser) || !$selected_advertiser->id)
                                        <td>
                                            {{ $entity->campaign->advertiser->name }}
                                        </td>
                                    @endif
                                    <td>
                                        {{ $entity->campaign->name }}
                                    </td>

                                    <td>
                                        {{ $entity->name }}
                                    </td>

                                    <td>
                                        {{ $entity->weight }}
                                    </td>
                                        <td>
                                            {{ $entity->zone->website->name }}
                                        </td>
                                    <td>
                                        {{ $entity->zone->name }}
                                    </td>

                                    <td style="width: 1%;white-space: nowrap;">
                                        {!! $entity->status ? '<span class="badge badge-info">Enable</span>'  : '<span class="badge badge-secondary">Disable</span>' !!}
                                    </td>
                                    <td class="text-right" style="width: 1%;white-space: nowrap;">
                                        @if (!$entity->status)
                                        <a type="button" class="btn btn-danger btn-xs waves-effect waves-themed"
                                           href="#" onclick="this.getElementsByTagName('form')[0].submit()">Delete
                                            <form style="display: none;" id="" action="{{ route('banners.delete' ,  [$entity->id]) }}" method="post">
                                                @method('delete')
                                                @csrf
                                            </form>
                                        </a>
                                        @endif

                                        <a type="button" class="btn btn-info btn-xs waves-effect waves-themed ml-2"
                                           href="{{ route('advertisers.campaigns.banners.edit' ,  [$entity->campaign->advertiser_id,$entity->campaign_id , $entity->id]) }}">Edit</a>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
