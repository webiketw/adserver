<?php

namespace App\Http\Livewire;

use App\Models\Website;
use App\Models\Zone;
use Livewire\Component;

class WebsiteSelector extends Component
{

    public $websiteId;
    public $zones;
    public $websites;
    public $website;

    public $count = 0;


    public function render()
    {

        $this->websites = Website::all();
        if ($this->websiteId){
            $this->website = Website::find($this->websiteId);
            $this->zones = $this->website->zones;
        }
        else
            $this->zones = [];

        $this->emitTo('zone-selector', 'change@website' , $this->websiteId);

        return view('livewire.website-selector');
    }
}
