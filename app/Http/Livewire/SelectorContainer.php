<?php

namespace App\Http\Livewire;

use App\Models\Banner;
use Livewire\Component;

class SelectorContainer extends Component
{

    public $websiteId;
    public $zoneId;
    public $banner;
    public $media;

    public function render()
    {
        if ($this->banner){
            $zone = $this->banner->zone;
            $website = $zone->website;

            $this->zoneId = $zone->id;
            $this->websiteId = $website->id;
            $this->media = $this->banner->media;
        }
        return view('livewire.selector-container');
    }
}
