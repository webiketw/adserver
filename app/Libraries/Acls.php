<?php


namespace App\Libraries;


use App\Entities\Acl;
use App\Models\Page;

class Acls
{
    private $acls;

    private $constraints;

    public function __construct( $acls )
    {
        $this->acls = $acls;
    }


    public function test( $constraints )
    {
        $this->constraints = $constraints;

        $acl = $this->getFirstAcl();
        return $this->getValue($acl);
    }

    private function getValue(Acl $acl)
    {
        if ($acl->getType() == 'constraint')
            return $acl->test($this->constraints);

        $result = [];

        $acls = $this->getChildAcls($acl);

        foreach ($acls as $_acl) {
            $result[] = $this->getValue($_acl);
        }

        if (!count($result))
            return true;

        if ($acl->getOperator() == 'OR') {
            foreach ($result as $value)
                if ($value)
                    return true;
        }

        if ($acl->getOperator() == 'AND') {
            foreach ($result as $value)
                if (!$value)
                    return false;
            return true;
        }

        return false;
    }



    private function getAclByKey($key)
    {
        foreach ($this->acls as $acl) {
            if ($acl->getKey() == $key)
                return $acl;
        }
    }

    private function getFirstAcl()
    {
        return $this->getAclByKey("condition_0");
    }

    private function getChildAcls(Acl $acl)
    {
        $key = $acl->getKey();
        $data = [];
        $pattern = "/^" . $key . '_[0-9]+$/i';
        foreach ($this->acls as $acl) {
            if (preg_match($pattern, $acl->getKey()))
                $data[] = $acl;
        }
        return $data;
    }
}
