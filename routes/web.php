<?php

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;


Route::get('login', [LoginController::class, 'redirectToProvider'])->name('login');
Route::get('login/callback', [LoginController::class, 'handleProviderCallback']);


Route::middleware(['auth'])->group(function () {

    Route::get('/', function () {
        return view('pages.home.index');
    })->name('home');


    Route::resources([
        'advertisers' => \App\Http\Controllers\AdvertiserController::class,
        'advertisers.campaigns' => \App\Http\Controllers\CampaignController::class,
        'advertisers.campaigns.banners' => \App\Http\Controllers\BannerController::class,
        'websites' => \App\Http\Controllers\WebsiteController::class,
        'websites.pages' => \App\Http\Controllers\PageController::class,
        'websites.zones' => \App\Http\Controllers\ZoneController::class,
        'rectangles' => \App\Http\Controllers\RectangleController::class,
    ]);

    Route::get('pages', [\App\Http\Controllers\PageController::class, 'index'])->name('pages.index');
    Route::get('zones', [\App\Http\Controllers\ZoneController::class, 'index'])->name('zones.index');

    Route::get('campaigns', [\App\Http\Controllers\CampaignController::class, 'index'])
        ->name('campaigns.index');

    Route::get('banners', [\App\Http\Controllers\BannerController::class, 'index'])
        ->name('banners.index');

    Route::delete('banners/{banner}', [\App\Http\Controllers\BannerController::class, 'destroy'])
        ->name('banners.delete');


    Route::get('banners/{banner}', [\App\Http\Controllers\BannerController::class, 'implicit'])
        ->name('banners.show');


    Route::get('advertisers/{advertiser}/banners', [\App\Http\Controllers\BannerController::class, 'index'])
        ->name('advertisers.banners.index');


    Route::get('advertisers/{advertiser}/banners/{campaign}/clone/{banner}', [\App\Http\Controllers\BannerController::class, 'clone'])
        ->name('advertisers.banners.clone');

});


Route::get('fetch', [\App\Http\Controllers\FetchController::class, 'index']);

