<?php

namespace App\Models;

use App\Libraries\AclFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    private $acls = null;

    protected $fillable = ['name' , 'weight' , 'campaign_id' , 'value' , 'redirect_to' , 'status' , 'alt' , 'width' , 'height'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }


    public function zone()
    {
        return $this->belongsTo(Zone::class);
    }



    public function acls()
    {
        if ($this->acls)
            return $this->acls;

        $this->acls = AclFactory::make($this->filter);
        return $this->acls;
    }

}
