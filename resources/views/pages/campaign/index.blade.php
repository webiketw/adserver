@extends('layouts.app')

@push('page-styles')
    <style type="text/css">
        #panel-1 .dropdown-item {
            padding: 0.3rem 1.5rem;
        }
    </style>
@endpush

@section('content')


    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        Campaign
                    </h2>

                    <div class="btn-group">
                        <button class="btn btn-secondary btn-xs dropdown-toggle" type="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Advertiser
                            : {{ (isset($selected_advertiser) && $selected_advertiser->id) ? $selected_advertiser->name : '請選擇' }}
                        </button>
                        @if (count($advertisers->except([$selected_advertiser->id])))
                            <div class="dropdown-menu">
                                @foreach($advertisers->except([$selected_advertiser->id]) as $advertiser)
                                    <a class="dropdown-item"
                                       href="{{ route('advertisers.campaigns.index' , [$advertiser->id]) }}">{{ $advertiser->name }}</a>
                                @endforeach
                            </div>
                        @endif
                    </div>

                    <div class="panel-toolbar">
                        @if (isset($selected_advertiser) && $selected_advertiser->id)
                            <a href="{{ route('advertisers.campaigns.create' , [$selected_advertiser->id]) }}"
                               type="button" class="btn btn-xs btn-success ml-3">Create new campaign</a>
                        @endif
                    </div>

                </div>

                <div class="panel-container show">
                    <div class="panel-content">
                        <table class="table table-bordered table-hover table-striped">
                            <thead class="bg-fusion-50">
                            @if (!isset($selected_advertiser) || !$selected_advertiser->id)
                                <th>
                                    Advertiser
                                </th>
                            @endif
                            <th>
                                Name
                            </th>
                            <th>
                                Priority
                            </th>

                            <th>
                                Activate
                            </th>
                            <th>
                                Expire
                            </th>
                            <th>
                                Action
                            </th>
                            </thead>
                            <tbody>
                            @foreach ($collection as $entity)
                                <tr>
                                    @if (!isset($selected_advertiser) || !$selected_advertiser->id)
                                        <td>
                                            {{ $entity->advertiser->name }}
                                        </td>
                                    @endif
                                    <td>
                                        <a href="{{ route('advertisers.campaigns.edit' , [ $entity->advertiser_id , $entity->id ]) }}">
                                            {{ $entity->name }}
                                        </a>

                                    </td>

                                    <td>
                                        {{ $entity->priority }}
                                    </td>

                                    <td>
                                        {{ $entity->activate_time }}
                                    </td>

                                    <td>
                                        {{ $entity->expire_time }}
                                    </td>
                                    <td class="text-right" style="width: 1%;white-space: nowrap;">
                                        <a type="button" class="btn btn-info btn-xs waves-effect waves-themed"
                                           href="{{ route('advertisers.campaigns.edit', [$entity->advertiser_id, $entity->id]) }}">
                                            Edit
                                        </a>

                                        <a type="button" class="btn btn-primary btn-xs waves-effect waves-themed  ml-2"
                                           href="{{ route('advertisers.campaigns.banners.create', [ $entity->advertiser_id , $entity->id]) }}">Add
                                            new banner</a>
                                        <a type="button" class="btn btn-info btn-xs waves-effect waves-themed ml-2"
                                           href="{{ route('advertisers.campaigns.banners.index', [ $entity->advertiser_id , $entity->id]) }}">
                                            banners
                                        </a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

        @endsection



        @push('page-scripts')
            <script type="text/javascript">


            </script>

    @endpush
