<div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label" for="banner-website_id">Website*</label>
        <livewire:website-selector :websiteId="$websiteId"/>
    </div>

    <div class="form-group row" style="margin-bottom: 1.5rem;">
        <label class="col-sm-2 col-form-label" for="banner-zone_id">Zone*</label>
        <livewire:zone-selector :zoneId="$zoneId" :websiteId="$websiteId" />
    </div>

    <div class="form-group row" style="margin-bottom: 1.5rem;">
        <label class="col-sm-2 col-form-label" for="banner-media">Media {{ $this->banner ? '' : '*'}}</label>
        <livewire:media-selector :zoneId="$zoneId" :media="$media" />
    </div>
</div>
