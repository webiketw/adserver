<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WebsiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'url' => 'https://www.webike.tw',
            'name' => '摩托百貨'
        ]);

        DB::table('users')->insert([
            'url' => 'https://news.webike.tw',
            'name' => '摩托新聞'
        ]);
    }
}
