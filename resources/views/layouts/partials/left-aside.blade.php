<aside class="page-sidebar">
    <div class="page-logo">
        <a href="{{ route('home') }}" class="page-logo-link d-flex align-items-center position-relative">
            <img src="{!! asset('smartadmin/img/logo.png') !!}" alt="SmartAdmin WebApp" aria-roledescription="logo">
            <span class="page-logo-text mr-1">Ats</span>
        </a>
    </div>
    <!-- BEGIN PRIMARY NAVIGATION -->
    <nav id="js-primary-nav" class="primary-nav" role="navigation">

        <ul id="js-nav-menu" class="nav-menu">

            <li>
                <a href="{{ route('advertisers.index') }}" title="Advertisers" >
                    <i class="fal fa-alien"></i>
                    <span class="nav-link-text" >Advertisers</span>
                </a>
            </li>

            <li>
                <a href="{{ route('campaigns.index') }}" title="Campaigns" >
                    <i class="fal fa-alien"></i>
                    <span class="nav-link-text" >Campaigns</span>
                </a>
            </li>

            <li>
                <a href="{{ route('banners.index') }}" title="Websites" >
                    <i class="fal fa-alien"></i>
                    <span class="nav-link-text" >Banners</span>
                </a>
            </li>

            <li class="nav-title">Management</li>

            <li>
                <a href="{{ route('websites.index') }}" title="Websites" >
                    <i class="fal fa-alien"></i>
                    <span class="nav-link-text" >Websites</span>
                </a>
            </li>

            <li>
                <a href="{{ route('pages.index') }}" title="Pages" >
                    <i class="fal fa-alien"></i>
                    <span class="nav-link-text" >Pages</span>
                </a>
            </li>

            <li>
                <a href="{{ route('zones.index') }}" title="Zones" >
                    <i class="fal fa-alien"></i>
                    <span class="nav-link-text" >Zones</span>
                </a>
            </li>


            <li class="nav-title">System</li>

            <li>
                <a href="{{ route('rectangles.index') }}" title="Rectangles" >
                    <i class="fal fa-alien"></i>
                    <span class="nav-link-text" >Rectangles</span>
                </a>
            </li>

            <li>
                <a href="/" title="Users" >
                    <i class="fal fa-alien"></i>
                    <span class="nav-link-text" >Users</span>
                </a>
            </li>


        </ul>

    </nav>
    <!-- END PRIMARY NAVIGATION -->

</aside>
