<?php

namespace App\Providers;

use App\Models\Advertiser;
use App\Models\Page;
use App\Models\Website;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        try{
            View::share('websites' , Website::with('pages' , 'zones')->get());
            View::share('advertisers' , Advertiser::with('campaigns')->get());
            View::share('pages' , Page::all());
        } catch (\Exception $ex){}


        url()->forceRootUrl(config('app.url'));
    }
}
