@push('page-styles')
    <style>
        .rule-nest li {
            margin-top: 10px;
        }

        .rule-nest {
            padding-left: 16px !important;
        }

        #filter_list .form-control {
            width: auto;
            display: inline-block;
            margin-left: 5px;
        }
    </style>

@endpush

<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="banner-status">Delivery</label>
    <div class="col-sm-10">
        <select class="form-control" id="banner-status" required name="status">
            <option value="1" {{ $banner->status ? 'selected' : '' }}>Enable</option>
            <option value="0" {{ !$banner->status ? 'selected' : '' }}>Disable</option>
        </select>
    </div>
</div>


<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="banner-status">Logic</label>
    <div class="col-sm-10 card-body">
        <div class="row">
            <div class="well" id="filter_list">
                <input type="hidden" name="filter[condition_0][type]" value="condition" id="condition_0_hidden">
                如果這些條件
                <select class="form-control" name="filter[condition_0][operator]">
                    <option value="AND">全部為真</option>
                    <option value="OR">之一為真</option>
                </select>
                <ul class="list-unstyled spaced rule-nest" id="condition_0_root">
                    <li id="condition_0_0">
                        <a href="#" class="add">
                            <i class="fal fa-plus-circle"></i>
                        </a>
                    </li>

                </ul>

            </div>
        </div>

    </div>

    <label class="col-sm-12 col-form-label" for="banner-comment">可在條件值後方加 * 代表包含該數值的資料</label><br/>
    <label class="col-sm-12 col-form-label" for="banner-comment">Ex. 3000-3001* 就會對應到 3000-3001、3000-3001-3006</label>

</div>


<template id="template-rule-select">
    <select class="filter_type form-control" name="choice_rule[]" required>
        <option>請選擇新增的條件</option>
        <option value="condition">組合條件</option>
        <option value="page" data-type="select">頁面</option>
        <option value="brand">品牌</option>
        <option value="category">分類</option>
        <option value="motor">車型</option>
        <option value="role" data-type="select" data-src="">身分</option>
    </select>
</template>

<template id="template-condition-select">
    <span class="input-icon input-icon-right">
        <input type="hidden" name="filter[{name_id}][type]" value="condition">
        <select class="form-control" name="filter[{name_id}][operator]">
            <option value="AND">全部為真</option>
            <option value="OR">之一為真</option>
        </select>
    </span>
    <a href="#" class="remove red2">
        <i class="ace-icon fal fa-trash-alt"></i>
    </a>
    <ul class="list-unstyled spaced rule-nest" id="{name_id}_root">
        <li id="{name_id}_0">
            <a href="#" class="add">
                <i class="ace-icon fal fa-plus-circle"></i>
            </a>
        </li>
    </ul>
</template>

<template id="template-operator-select">
    <label>{number}</label>：{text}
    <select class="form-control" name="filter[{name_id}][operator]" required>
        <option value="EQUAL">等於</option>
        <option value="NOT_EQUAL">不等於</option>
    </select>
</template>

<template id="template-add-select">
    <li id="{n_branch_id}">
        <a href="#" class="add">
            <i class="ace-icon fal fa-plus-circle"></i>
        </a>
    </li>
</template>

<template id="template-text-input">
    <span class="input-icon input-icon-right">
        <input class="form-control" type="text" style="width: 500px" name="filter[{name_id}][value]" required>
        <input type="hidden" name="filter[{name_id}][field]" value="{value}">
    </span>
    <a href="#" class="remove red2">
        <i class="ace-icon fal fa-trash-alt"></i>
    </a>
</template>

<template id="template-option-select">
    <span class="input-icon input-icon-right">
        <select class="form-control" style="width: 500px" name="filter[{name_id}][value]" required>
        </select>
        <input type="hidden" name="filter[{name_id}][field]" value="{value}">
    </span>
    <a href="#" class="remove red2">
        <i class="ace-icon fal fa-trash-alt"></i>
    </a>
</template>

@push('page-scripts')

    <script type="text/javascript">
        //// FILTER
        //add new filter
        $(document).on('click', '#filter_list .add', function (event) {
            let template = document.getElementById("template-rule-select");
            let clone = template.content.cloneNode(true);
            $(this).replaceWith(clone);
            return false;
        });

        //remove this filter
        $(document).on('click', '#filter_list .remove', function (event) {
            $(this).closest('li').remove();
            return false;
        });

        //choice filter type
        $(document).on('change', '#filter_list .filter_type:last', function (event) {
            let now_number = 1;
            let tree_id = 0;
            let n_branch_id = '';
            let html = "";
            let el = $('#filter_list ul li label:last');
            if (el.text()) {
                now_number = +el.text() + 1;
            }
            if ($(this).val()) {
                let name_id = $(this).closest('li').attr('id');
                tree_id = $(this).closest('ul').attr('id');
                let branch_id = $(this).closest('ul').find('li:last').attr('id');
                let branch_arr = branch_id.split('_');
                let last_element = branch_arr[branch_arr.length - 1].valueOf();
                last_element++
                branch_arr = branch_arr.slice(0, -1);
                for (let key in branch_arr) {
                    n_branch_id += branch_arr[key] + '_';
                }
                n_branch_id += last_element;
                if ($(this).val() == "condition") {
                    //text filter
                    html = document.getElementById("template-condition-select").innerHTML;
                    html = html.formatUnicorn({"name_id": name_id});

                    $(this).after(html);

                    html = document.getElementById("template-add-select").innerHTML;
                    html = html.formatUnicorn({
                        "n_branch_id": n_branch_id,
                    });
                    $(this).closest('ul').closest('ul').append(html);
                    $(this).replaceWith('<label>' + now_number + '</label>：如果這些條件');

                } else {

                    if ($(this).val() == 'page') {
                        html = document.getElementById("template-option-select").innerHTML;
                        html = html.formatUnicorn({
                            "name_id": name_id,
                            "value": $(this).val(),
                        });

                        html = $(html);
                        let selector = html.find('select')[0];
                        appendPageOptions(selector);
                    } else if ($(this).val() == 'role') {
                        html = document.getElementById("template-option-select").innerHTML;
                        html = html.formatUnicorn({
                            "name_id": name_id,
                            "value": $(this).val(),
                        });

                        html = $(html);
                        let selector = html.find('select')[0];
                        appendRoleOptions(selector);


                    } else if ($(this).find('option:selected').attr('data-type') == 'select') {
                        html = document.getElementById("template-option-select").innerHTML;
                        html = html.formatUnicorn({
                            "name_id": name_id,
                            "value": $(this).val(),
                        });


                    } else {
                        html = document.getElementById("template-text-input").innerHTML;
                        html = html.formatUnicorn({
                            "name_id": name_id,
                            "value": $(this).val(),
                        });
                    }


                    $(this).after(html);

                    html = document.getElementById("template-add-select").innerHTML;
                    html = html.formatUnicorn({
                        "n_branch_id": n_branch_id,
                    });
                    $(this).closest('ul').append(html);

                    html = document.getElementById("template-operator-select").innerHTML;
                    html = html.formatUnicorn({
                        "number": now_number,
                        "text": $(this).find('option:selected').text(),
                        "name_id": name_id
                    });


                    $(this).replaceWith(html);


                }

            }
        });

        //// FILTER END


        function appendPageOptions(el) {
            $.each(pages, function (index, value) {
                $(el).append($('<option>', {
                    "value": value.id,
                    "text": value.name
                }));
            });
        }

        function appendRoleOptions(el) {
            $.each(roles, function (index, value) {
                $(el).append($('<option>', {
                    "value": value.id,
                    "text": value.name
                }));
            });
        }
    </script>

    <script type="text/javascript">
        String.prototype.formatUnicorn = String.prototype.formatUnicorn ||
            function () {
                "use strict";
                let str = this.toString();
                if (arguments.length) {
                    let t = typeof arguments[0];
                    let key;
                    let args = ("string" === t || "number" === t) ?
                        Array.prototype.slice.call(arguments)
                        : arguments[0];

                    for (key in args) {
                        str = str.replace(new RegExp("\\{" + key + "\\}", "gi"), args[key]);
                    }
                }

                return str;
            };
    </script>

    <script type="text/javascript">

        const pages = JSON.parse('{!! $pages->toJson() !!}'.replace(/\r\n/g,"<br/>"));
        const roles = [
            {'id': 'USER', 'name': '已登入(棄用)'},
            {'id': 'ANONYMOUS', 'name': '未登入'},
            {'id': 'WHOLESALE', 'name': '經銷商'},
            {'id': 'CUSTOMER', 'name': '客戶'}
        ];

    </script>



    <script type="text/javascript">

        @if ($banner->filter)

        $(document).ready(function () {
            @foreach ($banner->acls() as $acl)

            @if ($loop->first)
                $("#filter_list select[name='filter[condition_0][operator]']").val('{{ $acl->getOperator() }}').change();

            @else

                @switch($acl->getType())
                    @case('constraint')
                        $("#filter_list{{ str_repeat(' ul.rule-nest' , count(explode("_" , $acl->getKey())) - 2) }}:last li > a.add").click();
                        $("#filter_list select.filter_type").val('{{ $acl->getField() }}').change();
                        $("#filter_list li > select:last").val('{{ $acl->getOperator() }}').change();
                        @if ($acl->getField() == 'page' || $acl->getField() == 'role')
                            $("#filter_list span > select:last").val('{{ $acl->getValue() }}').change();
                        @else
                            $("#filter_list span > input[type=text]:last").val('{{ $acl->getValue() }}').change();
                        @endif

                        @break

                    @case('condition')
                        $("#filter_list ul.rule-nest li:last > a.add").click();
                        $("#filter_list select.filter_type").val('condition').change();
                        $("#filter_list li span > select:last").val('{{ $acl->getOperator() }}').change();
                        @break

                    @default:
                alert('unknown type :  {{ $acl->getType() }}');

                @endswitch

            @endif
            @endforeach
        });


        @endif

    </script>

@endpush
