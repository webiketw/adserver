@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        New Banner
                    </h2>
                    <div class="btn-group">
                        <button class="btn btn-secondary btn-xs" type="button">
                            Advertiser : {{ $selected_advertiser->name}}
                        </button>
                    </div>
                    <div class="btn-group ml-3">
                        <button class="btn btn-secondary btn-xs" type="button">
                            Campaign : {{ $selected_campaign->name }}
                        </button>
                    </div>
                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <form method="POST" action="{{ route('advertisers.campaigns.banners.store' , [$selected_advertiser->id , $selected_campaign->id ]) }}"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="campaign_id" value="{{ $selected_campaign->id }}">

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="banner-name">Name*</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="banner-name" required
                                           value=""  name="name">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="banner-weight">Weight*</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="banner-weight"
                                           value="" step="0.1" name="weight" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="banner-alt">Alt text</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="banner-alt" required
                                           value="" name="alt">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="banner-redirect_to">Destination URL</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="banner-redirect_to" value="" required
                                           name="redirect_to"
                                    >
                                </div>
                            </div>



                            <livewire:selector-container :bannerId="$banner->id"/>


                            <input type="hidden" name="status" value="0" />


                            <div
                                class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row-reverse">
                                <button type="submit" class="btn btn-primary ml-3">Save changes</button>
                                <a type="button" class="btn btn-secondary ml-3" href="javascript:javascript:history.go(-1)">Close</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')


@endpush
