<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePage;
use App\Http\Requests\UpdatePage;
use App\Models\Page;
use App\Models\Website;
use Illuminate\Http\Request;

class PageController extends Controller
{

    public function index(Website $website = null)
    {
        if (!$website)
        {
            $selected_website = new Website();
            $collection = Page::with('website')->get();
        } else {
            $selected_website = $website;
            $collection = $website->pages()->get();
        }

        return view('pages.page.index', compact('collection' , 'selected_website'));
    }

    public function create(Website $website)
    {
        $selected_website = $website;
        $page = new Page();
        return view('pages.page.createOrUpdate' , compact('page' , 'selected_website'));
    }


    public function store(Website $website , StorePage $request)
    {

        $page = new Page();
        $values = $request->only($page->getFillable());
        $page->fill($values);
        $page->save();

        return redirect()->route('websites.pages.index' , [$website->id]);
    }


    public function show(Website $website , Page $page)
    {
        return redirect()->route('websites.pages.edit' , [$website->id , $page->id]);
    }

    public function edit(Website $website , Page $page)
    {
        $selected_website = $website;
        return view('pages.page.createOrUpdate' , compact('page' , 'selected_website'));
    }


    public function update(UpdatePage $request,Website $website , Page $page)
    {
        $values = $request->only($page->getFillable());
        $page->fill($values);
        $page->save();

        return redirect()->route('websites.pages.index' , [$website->id]);
    }


    public function destroy(Website $website , Page $page)
    {
        $page->delete();
        return redirect()->route('websites.pages.index' , [$website->id]);
    }
}
