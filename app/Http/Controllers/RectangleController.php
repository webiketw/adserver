<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRectangle;
use App\Http\Requests\UpdateRectangle;
use App\Models\Rectangle;
use Illuminate\Http\Request;

class RectangleController extends Controller
{

    public function index()
    {
        $collection = Rectangle::with('zones')->get();
        return view('pages.rectangle.index', compact('collection'));
    }


    public function create()
    {
        $rectangle = new Rectangle();
        return view('pages.rectangle.createOrUpdate' , compact('rectangle'));
    }


    public function store(StoreRectangle $request)
    {
        $rectangle = new Rectangle();
        $values = $request->only($rectangle->getFillable());
        $rectangle->fill($values);
        $rectangle->save();

        return redirect()->route('rectangles.index');
    }


    public function show(Rectangle $rectangle)
    {
        return redirect()->route('rectangles.edit' , [$rectangle->id]);
    }


    public function edit(Rectangle $rectangle)
    {
        return view('pages.rectangle.createOrUpdate' , compact('rectangle'));
    }


    public function update(UpdateRectangle $request, Rectangle $rectangle )
    {
        $values = $request->only($rectangle->getFillable());
        $rectangle->fill($values);
        $rectangle->save();
        return redirect()->route('rectangles.index');
    }


    public function destroy(Rectangle $rectangle)
    {
        //
    }
}
