<?php

namespace App\Http\Livewire;

use App\Models\Rectangle;
use App\Models\Zone;
use Livewire\Component;

class MediaSelector extends Component
{

    public $zoneId;
    public $media;

    protected $listeners = [
        'change@zone' => 'change'
    ];


    public function change($zone_id)
    {
        $this->zoneId =$zone_id;
    }

    public function getRectangleProperty()
    {
        if ($this->zoneId)
            return Zone::with('rectangle')->find($this->zoneId)->rectangle;
        else
            return null;

    }

    public function render()
    {
        return view('livewire.media-selector');
    }
}
