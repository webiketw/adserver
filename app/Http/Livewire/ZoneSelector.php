<?php

namespace App\Http\Livewire;

use App\Models\Website;
use Livewire\Component;

class ZoneSelector extends Component
{
    public $zones;
    public $zone;
    public $zoneId;
    public $websiteId;

    protected $listeners = ['change@website' => 'change'];

    public function change($websiteId)
    {
        $this->websiteId =$websiteId;
    }

    public function render()
    {

        if ($this->websiteId){
            $this->zones = Website::with('zones.rectangle')->find($this->websiteId)->zones;
            $this->zone = $this->zones->where('id' , $this->zoneId)->first();
        }
        else
            $this->zones = [];

        $this->emitTo('media-selector', 'change@zone' , $this->zoneId);

        return view('livewire.zone-selector' );
    }
}
