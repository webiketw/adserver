@extends('layouts.app')

@section('content')

    @php
        if ($page->id){
            $route = route('websites.pages.update' , [$selected_website->id , $page->id]) ;
            $method='PUT';
            $title = 'Edit';
        } else {
            $route = route('websites.pages.store' , [$selected_website->id]);
            $method='POST';
            $title = 'New';
        }
    @endphp

    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        {{ $title }} Page
                    </h2>

                    <div class="btn-group">
                        <button class="btn btn-secondary btn-xs" type="button">
                            Website : {{ $selected_website->name }}
                        </button>
                    </div>
                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <form method="POST" action="{{ $route }}">
                            @csrf
                            @method($method)

                            <input type="hidden" name="website_id" value="{{ $selected_website->id }}" />

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="page-name">Name*</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="page-name" name="name"
                                           value="{{ old('name') ?? $page->name }}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="page-expression">Expression*</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="page-expression" name="expression"
                                           value="{{ old('expression') ?? $page->expression }}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="page-description">Description</label>
                                <div class="col-sm-10">
                                    <input type="url" class="form-control" id="page-description" name="description"
                                           value="{{ old('description') ?? $page->description }}" >
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="page-comments">Comments</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="page-comments"
                                              name="comments">{!! old('comments') ?? $page->comments !!}</textarea>
                                </div>
                            </div>


                            <div
                                class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row-reverse">
                                <button type="submit" class="btn btn-primary ml-3">Save changes</button>
                                <a type="button" class="btn btn-secondary ml-3" href="javascript:javascript:history.go(-1)">Close</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
