<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAdvertiser;
use App\Http\Requests\UpdateAdvertiser;
use App\Models\Advertiser;

class AdvertiserController extends Controller
{

    public function index()
    {
        $collection = Advertiser::all();
        return view('pages.advertiser.index' , compact('collection'));
    }

    public function create()
    {
        $advertiser = new Advertiser();
        return view('pages.advertiser.createOrUpdate' , compact('advertiser'));
    }


    public function store(StoreAdvertiser $request)
    {

        $advertiser = new Advertiser();
        $advertiser->name = $request->get('name');
        $advertiser->contact = $request->get('contact');
        $advertiser->email = $request->get('email');
        $advertiser->save();

        return redirect()->route('advertisers.index');
    }

    public function show(Advertiser $advertiser)
    {
        return redirect()->route('advertisers.edit' , [$advertiser->id]);
    }

    public function edit(Advertiser $advertiser)
    {
        return view('pages.advertiser.createOrUpdate' , compact('advertiser'));
    }


    public function update(UpdateAdvertiser $request, Advertiser $advertiser)
    {
        $advertiser->name = $request->get('name');
        $advertiser->contact = $request->get('contact');
        $advertiser->email = $request->get('email');
        $advertiser->save();
        return redirect()->route('advertisers.index');
    }

    public function destroy(Advertiser $advertiser)
    {
        $advertiser->delete();
        return redirect()->route('advertisers.index');
    }
}
