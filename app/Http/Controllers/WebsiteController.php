<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreWebsite;
use App\Http\Requests\UpdateWebsite;
use App\Models\Advertiser;
use App\Models\Website;
use Illuminate\Http\Request;

class WebsiteController extends Controller
{
    public function index()
    {
        $collection = Website::withCount('pages')->withCount('zones')->get();
        return view('pages.website.index' , compact('collection'));
    }

    public function create()
    {
        $website = new Website();
        return view('pages.website.createOrUpdate' , compact('website'));
    }


    public function store(StoreWebsite $request)
    {
        $website = new Website();
        $values = $request->only($website->getFillable());
        $website->fill($values);
        $website->save();

        return redirect()->route('websites.index');
    }

    public function show(Website $website)
    {
        return redirect()->route('websites.edit' , [$website->id]);
    }


    public function edit(Website $website)
    {
        return view('pages.website.createOrUpdate' , compact('website'));
    }


    public function update(UpdateWebsite $request, Website $website)
    {
        $values = $request->only($website->getFillable());
        $website->fill($values);
        $website->save();
        return redirect()->route('websites.index');
    }


    public function destroy(Website $website)
    {
        $website->delete();
        return redirect()->route('websites.index');
    }
}
