<?php


namespace App\Http\Controllers;


use App\Http\Requests\FetchRequest;
use App\Libraries\AclFactory;
use App\Libraries\Acls;
use App\Models\Website;
use Illuminate\Support\Facades\Storage;

class FetchController extends Controller
{
    /*
     * refer : https://www.webike.tw/ca/3000-3001/br/2573
     * category : 3000-3001
     * role : ANONYMOUS
     * brand : 2573
     * motor : null
     * zone : 1
     */

    /*
     * refer : https://www.webike.tw/ca/3000-3001/br/2573
     * website : https://www.webike.tw
     * path : /ca/3000-3001/br/2573
     * category : 3000-3001
     * role : ANONYMOUS
     * brand : 2573
     * motor : null
     * zone : 1
     */

    public function index(FetchRequest $request)
    {

        $object = (object)$request->validationData();

        $this->normalization($object);

        $website = Website::with('zones.banners.campaign'  )->where('url' , $object->website)->first();
        if (!$website)
            return response()->json((object)['success'=>false,'message' => 'Undefined website : ' . $object->website]);

        $zone = $website->zones->where('uuid' , $object->zone)->first();


        if (!$zone)
            return response()->json((object)['success'=>false,'message' => 'Unknown zone : ' . $object->zone ]);

        // $banners = $zone->banners()->with('campaign')->get();


        $banners = $zone->banners()->whereHas('campaign', function($innerQuery) {
            $innerQuery
                ->where('activate_time', '<=', date('Y-m-d 00:00:00'))
                ->where('expire_time', '>=', date('Y-m-d 23:59:59'));
        })
            ->with('campaign')->get();

        if (!count($banners))
            return response()->json([]);




        $result = [];

        foreach ($banners as $banner){
            if (!$banner->status)
                continue;

            $acls = new Acls(AclFactory::make($banner->filter));

            $test = $acls->test($object);
            if ($test){
                if ($banner->zone->rectangle->type == 'image')
                    $media = Storage::disk('public')->url($banner->media);
                else
                    $media = $banner->media;

                $result[] = (object)[
                    'media' => $media,
                    'redirect_to' => $banner->redirect_to,
                    'alt' => $banner->alt,
                    'width' => $banner->width,
                    'height' => $banner->height ,
                    'weight' => $banner->weight * $banner->campaign->priority
                ];
            }
        }


        $this->weightRandSort($result);

        return response()->json($result);
    }


    private function weightRandSort(&$data)
    {

        $sum = 0;
        foreach ($data as $value)
            $sum += $value->weight;

        foreach ($data as &$value){
            $seed = 1 / rand(1,100);
            $value->calculate_weight = -log($seed) / (1/($value->weight/$sum));
        }


        usort($data , function($value_a,$value_b)
        {
            if ($value_a->calculate_weight > $value_b->calculate_weight)
                return -1;
            return 1;
        });


    }


    private function normalization(&$object)
    {
        $request_parses = parse_url($object->refer);
        $object->website = $request_parses['scheme'] . '://' . $request_parses['host'];
        if (!isset($request_parses['path']))
            $object->path = "/";
        else
            $object->path = $request_parses['path'];
    }
}
