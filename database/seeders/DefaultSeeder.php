<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->websites();
    }

    private function pages()
    {
        DB::table('pages')->insert([
            'name' => '百貨首頁',
            'expression' => '\/shopping',
            'comments' => 'https://www.webike.tw/shopping',
            'website_id' => 1
        ]);

        DB::table('pages')->insert([
            'name' => '百貨首頁',
            'expression' => '\/shopping',
            'comments' => 'https://www.webike.tw/shopping',
            'website_id' => 1
        ]);
        DB::table('pages')->insert([
            'name' => '百貨首頁',
            'expression' => '\/shopping',
            'comments' => 'https://www.webike.tw/shopping',
            'website_id' => 1
        ]);
        DB::table('pages')->insert([
            'name' => '百貨首頁',
            'expression' => '\/shopping',
            'comments' => 'https://www.webike.tw/shopping',
            'website_id' => 1
        ]);
        DB::table('pages')->insert([
            'name' => '百貨首頁',
            'expression' => '\/shopping',
            'comments' => 'https://www.webike.tw/shopping',
            'website_id' => 1
        ]);
        DB::table('pages')->insert([
            'name' => '百貨首頁',
            'expression' => '\/shopping',
            'comments' => 'https://www.webike.tw/shopping',
            'website_id' => 1
        ]);
        DB::table('pages')->insert([
            'name' => '百貨首頁',
            'expression' => '\/shopping',
            'comments' => 'https://www.webike.tw/shopping',
            'website_id' => 1
        ]);
        DB::table('pages')->insert([
            'name' => '百貨首頁',
            'expression' => '\/shopping',
            'comments' => 'https://www.webike.tw/shopping',
            'website_id' => 1
        ]);
        DB::table('pages')->insert([
            'name' => '百貨首頁',
            'expression' => '\/shopping',
            'comments' => 'https://www.webike.tw/shopping',
            'website_id' => 1
        ]);
        DB::table('pages')->insert([
            'name' => '百貨首頁',
            'expression' => '\/shopping',
            'comments' => 'https://www.webike.tw/shopping',
            'website_id' => 1
        ]);


    }
    private function websites(){
        DB::table('websites')->insert([
            'url' => 'https://www.webike.tw',
            'name' => '摩托百貨'
        ]);

        DB::table('users')->insert([
            'url' => 'https://news.webike.tw',
            'name' => '摩托新聞'
        ]);
    }
}
