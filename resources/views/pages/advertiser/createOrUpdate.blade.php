@extends('layouts.app')

@section('message')
    @if ($errors->any())
        <div class="alert alert-primary alert-dismissible">
            @php
              $class = 'alert alert-danger';
            @endphp

            @foreach($errors->all() as $error)

                @if ($loop->last)
                    @php
                    $class .= ' mb-0';
                    @endphp
                @endif
                <div class="{{ $class }}" role="alert">
                    {{ $error }}
                </div>
            @endforeach
        </div>
    @endif

@endsection

@section('content')
    @php
        if ($advertiser->id){
            $route = route('advertisers.update' , [$advertiser->id]) ;
            $method='PUT';
            $title = 'Edit';
        } else {
            $route = route('advertisers.store');
            $method='POST';
            $title = 'New';
        }
    @endphp

    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        {{ $title }} Advertiser
                    </h2>

                </div>
                <div class="panel-container show">
                    <div class="panel-content">

                        <form method="POST" action="{{ $route }}">
                            @csrf
                            @method($method)

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="advertiser-name">Name*</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="name"
                                           id="advertiser-name" value="{{ old('name') ?? $advertiser->name }}" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="advertiser-contact">Contact</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="contact"
                                           id="advertiser-contact" value="{{ old('contact') ?? $advertiser->contact }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="advertiser-email">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" name="email"
                                           id="advertiser-email" value="{{ old('email') ?? $advertiser->email  }}">
                                </div>
                            </div>

                            <div class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row-reverse">
                                <button type="submit" class="btn btn-primary ml-3">Save changes</button>
                                <a type="button" class="btn btn-secondary ml-3" href="javascript:javascript:history.go(-1)">Close</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
