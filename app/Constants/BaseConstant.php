<?php


namespace App\Constants;


use ReflectionClass;

abstract class BaseConstant
{

    public static function getConstants()
    {
        $reflection = new ReflectionClass(static::class);
        return $reflection->getConstants();
    }

    public static function getName($value)
    {
        $constants = array_flip(static::getConstants());
        return $constants[$value];
    }
}
