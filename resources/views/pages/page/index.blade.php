@extends('layouts.app')

@push('page-styles')
    <style type="text/css">
        #panel-1 .dropdown-item {
            padding: 0.3rem 1.5rem;
        }
    </style>
@endpush

@section('content')


    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        Page
                    </h2>

                    <div class="btn-group">
                        <button class="btn btn-secondary btn-xs dropdown-toggle" type="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Website
                            : {{ (isset($selected_website) && $selected_website->id) ? $selected_website->name : '請選擇' }}
                        </button>
                        @if (count($websites->except([$selected_website->id])))
                            <div class="dropdown-menu">
                                @foreach($websites->except([$selected_website->id]) as $website)
                                    <a class="dropdown-item"
                                       href="{{ route('websites.pages.index' , [$website->id]) }}">{{ $website->name }}</a>
                                @endforeach
                            </div>
                        @endif
                    </div>

                    <div class="panel-toolbar">

                        @if (isset($selected_website) && $selected_website->id)
                            <a type="button" class="btn btn-xs btn-success ml-3"
                               href="{{ route('websites.pages.create' , [$selected_website->id]) }}">Create new page</a>
                        @endif

                    </div>

                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <table class="table table-bordered table-hover table-striped">
                            <thead class="bg-fusion-50">
                            @if (!isset($selected_website) || !$selected_website->id)
                                <th>
                                    Website
                                </th>
                            @endif
                            <th>
                                Name
                            </th>
                            <th>
                                Expression
                            </th>

                            <th>
                                Description
                            </th>
                            <th>
                                Comments
                            </th>
                            </thead>
                            <tbody>
                            @foreach ($collection as $entity)
                                <tr>
                                    @if (!isset($selected_website) || !$selected_website->id)
                                        <td>
                                            {{ $entity->website->name }}
                                        </td>
                                    @endif
                                    <td>
                                        <a href="{{ route('websites.pages.edit' , [$entity->website->id , $entity->id]) }}">
                                            {{ $entity->name }}
                                        </a>

                                    </td>

                                    <td>
                                        {{ $entity->expression }}
                                    </td>

                                    <td>
                                        {{ $entity->description }}
                                    </td>
                                    <td>
                                        {{ $entity->comments }}
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
