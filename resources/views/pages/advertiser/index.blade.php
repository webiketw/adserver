@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        Advertiser
                    </h2>

                    <div class="panel-toolbar">
                        <a href="{{ route('advertisers.create') }}" type="button" class="btn btn-xs btn-success ml-3">Create new advertiser</a>
                    </div>

                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <table class="table table-sm table-striped">
                            <thead class="bg-fusion-50">
                            <th>
                                Name
                            </th>
                            <th>
                                Contact
                            </th>
                            <th>
                                Email
                            </th>
                            <th></th>
                            </thead>
                            <tbody>
                            @foreach ($collection as $entity)
                                <tr>

                                    <td style="width: 40%;">
                                        <a href="{{ route('advertisers.edit' ,[$entity->id]) }}">
                                            {{ $entity->name }}
                                        </a>

                                    </td>

                                    <td>
                                        {{ $entity->contact }}
                                    </td>

                                    <td>
                                        {{ $entity->email }}
                                    </td>


                                    <td class="text-right">
                                        <a type="button" class="btn btn-primary btn-xs waves-effect waves-themed"
                                           href="{{ route('advertisers.campaigns.create' , [$entity->id]) }}">Add new campaign</a>
                                        <a type="button" class="btn btn-info btn-xs waves-effect waves-themed ml-2"
                                           href="{{ route('advertisers.campaigns.index' , [$entity->id]) }}">Campaigns</a>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
