<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="banner-name">Name*</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="banner-name" required
               value="{{ $banner->name }}"  name="name">
    </div>
</div>


<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="banner-weight">Weight*</label>
    <div class="col-sm-10">
        <input type="number" class="form-control" id="banner-weight"
               value="{{ $banner->weight }}" step="0.1" name="weight" required>
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="banner-alt">Alt text</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="banner-alt" required
               value="{{ $banner->alt }}" name="alt">
    </div>
</div>

<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="banner-redirect_to">Destination URL</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="banner-redirect_to" required
               name="redirect_to" value="{{ $banner->redirect_to }}" >
    </div>
</div>



<livewire:selector-container :bannerId="$banner->id" :banner="$banner" />


