<?php

namespace App\Http\Controllers;

use App\Constants\RectangleType;
use App\Models\Advertiser;
use App\Models\Banner;
use App\Models\Campaign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Webpatser\Uuid\Uuid;


class BannerController extends Controller
{

    public function index(Advertiser $advertiser = null, Campaign $campaign = null)
    {
        $collection = [];
        if (!$advertiser)
            $selected_advertiser = new Advertiser();
        else
            $selected_advertiser = $advertiser;


        if (!$campaign)
            $selected_campaign = new Campaign();
        else
            $selected_campaign = $campaign;

        if ($campaign)
            $collection = $campaign->banners;
        else if ($advertiser)
            $collection = $advertiser->banners;
        else
            $collection = Banner::with('campaign.advertiser')->get();

        return view('pages.banner.index', compact('collection', 'selected_advertiser', 'selected_campaign'));
    }


    public function create(Advertiser $advertiser, Campaign $campaign)
    {

        $selected_advertiser = $advertiser;
        $selected_campaign = $campaign;
        $banner_id =session()->pull('clone-banner', null);

        if ($banner_id){
            $banner = Banner::find($banner_id)->replicate();
            $banner->name = '(clone)' . $banner->name;
            $banner->status = 0;

            return view('pages.banner.clone', compact('banner', 'selected_advertiser', 'selected_campaign'));
        } else {
            $banner = new Banner();
            return view('pages.banner.create', compact('banner', 'selected_advertiser', 'selected_campaign'));
        }


    }


    public function store(Request $request, Advertiser $advertiser, Campaign $campaign)
    {

        $banner = new Banner();
        $banner = $this->storeBanner($request, $banner);

        return redirect()->route('advertisers.campaigns.banners.index', [$advertiser->id, $campaign->id, $banner->id]);
    }


    public function implicit(Banner $banner){
        return redirect()->route('advertisers.campaigns.banners.edit', [
            $banner->campaign->advertiser->id,
            $banner->campaign->id,
            $banner->id
        ]);
    }


    public function show(Advertiser $advertiser, Campaign $campaign, Banner $banner)
    {
        return redirect()->route('advertisers.campaigns.banners.edit', [
            $advertiser->id,
            $campaign->id,
            $banner->id
        ]);
    }

    public function edit(Advertiser $advertiser, Campaign $campaign, Banner $banner)
    {
        $banner->acls();
        $selected_advertiser = $advertiser;
        $selected_campaign = $campaign;
        $pages = $banner->zone->website->pages;

        return view('pages.banner.update', compact('banner', 'selected_advertiser',
            'selected_campaign', 'pages'));
    }


    public function update(Request $request, Advertiser $advertiser, Campaign $campaign, Banner $banner)
    {

        $banner = $this->storeBanner($request, $banner);
        return redirect()->route('advertisers.campaigns.banners.index', [$advertiser->id, $campaign->id, $banner->id]);

    }


    public function destroy(Banner $banner): \Illuminate\Http\RedirectResponse
    {
        $banner->delete();
        return redirect()->back();
    }


    public function clone(Advertiser $advertiser, Campaign $campaign, Banner $banner){
        session()->flash('clone-banner' , $banner->id);
        return redirect()->route('advertisers.campaigns.banners.create' , [$advertiser->id , $campaign->id]);
    }


    private function storeBanner(Request $request, Banner $banner)
    {
        $banner->name = $request->name;
        $banner->weight = $request->get('weight');
        $banner->campaign_id = $request->get('campaign_id');
        $banner->zone_id = $request->get('zone_id');
        $banner->redirect_to = $request->get('redirect_to');
        $banner->status = $request->get('status');
        $banner->alt = $request->get('alt');
        $banner->width = 0;
        $banner->height = 0;
        $banner->status = $request->get('status');

        switch ($request->get('media-type')) {
            /*
            case RectangleType::IMAGE:
                $file = $request->file('media');
                if ($file){
                    $ext = strtolower($file->getClientOriginalExtension());
                    $filename = (string)Uuid::generate() . '.' . $ext;
                    $path = $file->storeAs('images' , $filename  , 'public');
                    $this->setWidthAndHeight($banner , Storage::disk('public')->url($path));
                    $banner->media = $path;
                }
                break;
            */
            case RectangleType::URL:
                $this->setWidthAndHeight($banner , $request->get('media'));
                $banner->media = $request->get('media');
                break;

            default:
                $banner->width = 0;
                $banner->height = 0;
                $banner->media = $request->get('media');
        }


        $filter = $request->get('filter', null);
        if ($filter)
            $banner->filter = json_encode($filter);
        else
            $banner->status = 0;

        $banner->save();
        return $banner;
    }


    private function setWidthAndHeight(&$banner , $url){
        list($width, $height) = getimagesize($url);
        $banner->width = $width;
        $banner->height = $height;
    }
}
