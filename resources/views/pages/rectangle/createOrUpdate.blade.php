@extends('layouts.app')

@section('content')
    @php
        if ($rectangle->id){
            $route = route('rectangles.update' , [$rectangle->id]) ;
            $method='PUT';
            $title = 'Edit';
        } else {
            $route = route('rectangles.store');
            $method='POST';
            $title = 'New';
        }
    @endphp

    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        {{ $title }} Rectangle
                    </h2>

                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <form method="POST" action="{{ $route }}">
                            @csrf
                            @method($method)


                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="rectangle-width">Width*</label>
                                <div class="col-sm-10">
                                    <input name="width" type="number" class="form-control"
                                           id="rectangle-width" value="{{ old('width') ?? $rectangle->width }}"
                                           required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="rectangle-height">Height*</label>
                                <div class="col-sm-10">
                                    <input name="height" type="number" class="form-control"
                                           id="rectangle-height" value="{{ old('height') ?? $rectangle->height }}"
                                           required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="rectangle-type">Type*</label>
                                <div class="col-sm-10">
                                    <select name="type" class="form-control" id="rectangle-type" required>
                                        <option value="">-----</option>
                                        @foreach (\App\Constants\RectangleType::getConstants() as $key => $value)
                                            <option value="{{ $value }}" {{ $rectangle->type == $value ? 'selected' : '' }}>
                                                {{ $key }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="rectangle-comments">Comments</label>
                                <div class="col-sm-10">
                                    <textarea name="comments" class="form-control" id="rectangle-comments"
                                              rows="3">{{ old('comments') ?? $rectangle->comments }}</textarea>
                                </div>
                            </div>


                            <div
                                class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row-reverse">
                                <button type="submit" class="btn btn-primary ml-3">Save changes</button>
                                <a type="button" class="btn btn-secondary ml-3" href="javascript:javascript:history.go(-1)">Close</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
