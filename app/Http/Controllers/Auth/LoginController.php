<?php


namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/picker/scan-item';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->with(['hd' => 'everglory.asia'])->redirect();
    }


    public function handleProviderCallback()
    {
        $google_account = Socialite::driver('google')->user();

        if (!strpos($google_account->email , 'everglory.asia')){
            //return redirect()->back();
        }

        $user = User::where('email' , $google_account->email)->first();

        if (!$user){
            $user = new User();
            $user->name = $google_account->name;
            $user->email = $google_account->email;
        }
        $user->password = '';
        $user->save();

        Auth::login($user, true);


        return redirect()->route('home');
    }
}
