<?php


namespace App\Entities;


use App\Models\Page;

class Acl
{
    private $object;

    public function __construct($object)
    {
        $this->object = $object;
    }

    public function getKey()
    {
        return $this->object->key;
    }


    public function getType()
    {
        return $this->object->type;
    }


    public function getOperator()
    {
        return $this->object->operator;
    }


    public function getField()
    {
        if (!property_exists($this->object , 'field'))
            return null;

        return $this->object->field;
    }


    public function getValue()
    {
        if (!property_exists($this->object , 'value'))
            return null;

        return $this->object->value;
    }




    public function test( $values )
    {

        switch ($this->getField()) {
            case 'page':
                return $this->testPage($values);
            case 'role':
                return $this->testRole($values);
            default:
                return $this->testDefault($values);
        }
    }

    private function testRole($values)
    {
        $value = $values->role;
        switch ($this->getValue()){
            case 'USER':
                if ($value != 'ANONYMOUS')
                    $value = 'USER';
                return $this->testEqual($value , $this->getValue());
            default:
                return $this->testEqual($value , $this->getValue());
        }
    }

    private function testPage($values)
    {
        $value = $values->path;
        $page = Page::find($this->getValue());

        $value_0 = 0;
        if ($page){
            $pattern = '/' . $page->expression . '/i';
            $value_0 = preg_match($pattern, $value);
        }

        return $this->testEqual($value_0 ,1);
    }

    private function testDefault($values)
    {
        $field = $this->getField();
        $value = null;
        if (isset($values->$field)){
            $value = $values->$field;
        }


        $value_1 = $this->getValue();
        return $this->testEqual($value ,$value_1);
    }

    private function testEqual($value_0 , $value_1)
    {
        $operator = $this->getOperator();
        switch ($operator){
            case 'EQUAL':
                if ((substr($value_1 , -1)) === '*'){
                    $value_1 = substr($value_1 , 0 , -1);
                    $pos = strpos($value_0 , $value_1);
                    return $pos !== false;
                } else {
                    return $value_0 == $value_1;
                }

            case 'NOT_EQUAL':
                if ((substr($value_1 , -1)) === '*'){

                    $value_1 = substr($value_1 , 0 , -1);
                    $pos = strpos($value_0 , $value_1);

                    return $pos === false;
                } else {
                    return $value_0 != $value_1;
                }

            default:
                return false;
        }

    }

}
