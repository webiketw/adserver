<?php


namespace App\Libraries;


use App\Entities\Acl;

class AclFactory
{
    public static function make($string)
    {
        if (!$string)
            return null;

        $data = [];
        $objects = json_decode($string);

        foreach ($objects as $key => $object){
            $data[] = new Acl(
                (object)
                [
                    'key' => $key,
                    'type' => property_exists($object ,'type') ? $object->type : 'constraint',
                    'operator' => $object->operator,
                    'field' => property_exists($object ,'field') ? $object->field : null,
                    'value' => property_exists($object ,'value') ? $object->value : null,
                ]
            );

        }

        return $data;
    }
}
