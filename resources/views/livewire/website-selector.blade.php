<div class="col-sm-10">
    <select class="form-control" id="banner-website_id" required wire:model="websiteId">
        <option value="">-----</option>
        @foreach($websites as $website)
            <option value="{{ $website->id }}">{{ $website->name }}</option>
        @endforeach
    </select>

    <span class="help-block">{{ $this->website ? $this->website->url : '' }}</span>
</div>



