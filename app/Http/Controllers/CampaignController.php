<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCampaign;
use App\Http\Requests\UpdateCampaign;
use App\Models\Advertiser;
use App\Models\Campaign;
use Illuminate\Http\Request;

class CampaignController extends Controller
{

    public function index(Advertiser $advertiser = null)
    {
        if (!$advertiser)
        {
            $selected_advertiser = new Advertiser();
            $collection = Campaign::with('advertiser')->get();
        } else {
            $selected_advertiser = $advertiser;
            $collection = $advertiser->campaigns()->get();
        }
        return view('pages.campaign.index', compact('collection' , 'selected_advertiser'));
    }


    public function create(Advertiser $advertiser)
    {
        $selected_advertiser = $advertiser;
        $campaign = new Campaign();
        return view('pages.campaign.createOrUpdate' , compact('selected_advertiser' , 'campaign'));
    }


    public function store(Advertiser $advertiser , StoreCampaign $request)
    {

        $campaign = new Campaign();
        $values = $request->only($campaign->getFillable());
        $values['activate_time'] .= ' 00:00:00';
        $values['expire_time'] .= ' 23:59:59';
        $campaign->fill($values);
        $campaign->save();

        return redirect()->route('advertisers.campaigns.index' , [$advertiser->id]);
    }


    public function show(Advertiser $advertiser , Campaign $campaign)
    {
        return redirect()->route('advertisers.campaigns.edit' , [$advertiser->id , $campaign->id]);
    }


    public function edit(Advertiser $advertiser , Campaign $campaign)
    {
        $selected_advertiser = $advertiser;
        return view('pages.campaign.createOrUpdate' , compact('campaign' , 'selected_advertiser'));
    }


    public function update(UpdateCampaign $request, Advertiser $advertiser , Campaign $campaign)
    {
        $values = $request->only($campaign->getFillable());
        $values['activate_time'] .= ' 00:00:00';
        $values['expire_time'] .= ' 23:59:59';
        $campaign->fill($values);
        $campaign->save();

        return redirect()->route('advertisers.campaigns.index' , [$advertiser->id]);
    }


    public function destroy($id)
    {
        //
    }
}
