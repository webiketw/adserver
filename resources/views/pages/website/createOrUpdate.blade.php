@extends('layouts.app')

@section('content')

    @php
        if ($website->id){
            $route = route('websites.update' , [$website->id]) ;
            $method='PUT';
            $title = 'Edit';
        } else {
            $route = route('websites.store');
            $method='POST';
            $title = 'New';
        }
    @endphp

    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        {{ $title }} Website
                    </h2>

                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <form method="POST" action="{{ $route }}">
                            @csrf
                            @method($method)

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="website-name">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="website-name"
                                           name="name" value="{{ old('name') ?? $website->name }}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="website-url">Url</label>
                                <div class="col-sm-10">
                                    <input type="url" class="form-control" id="website-url"
                                           name="url" value="{{ old('url') ?? $website->url }}" required>
                                </div>
                            </div>

                            <div
                                class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row-reverse">
                                <button type="submit" class="btn btn-primary ml-3">Save changes</button>
                                <a type="button" class="btn btn-secondary ml-3" href="javascript:javascript:history.go(-1)">Close</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
