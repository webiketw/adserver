@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        Rectangle
                    </h2>

                    <div class="panel-toolbar">
                        <a href="{{ route('rectangles.create') }}" type="button" class="btn btn-xs btn-success ml-3">Create new rectangle</a>
                    </div>
                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <table class="table table-bordered table-hover">
                            <thead>
                            @foreach ($collection as $entity)
                                <tr>
                                    <td>
                                        {{ $entity->name }}
                                        @if ($entity->comments)
                                            <br/>
                                            <span style="color:gray">
                                                {{ $entity->comments }}
                                            </span>

                                        @endif
                                    </td>

                                    <td>
                                        @foreach ($entity->zones as $zone)
                                            <span class="badge badge-secondary">
                                                {{ $zone->name }}
                                            </span>

                                        @endforeach
                                    </td>

                                    <td class="text-right" style="width: 1%;white-space: nowrap;">
                                        <a type="button" class="btn btn-info btn-xs waves-effect waves-themed ml-2"
                                           href="{{ route('rectangles.edit', [$entity->id]) }}">
                                            Edit
                                        </a>
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
