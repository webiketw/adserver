@extends('layouts.app')

@section('content')

    @php
        if ($zone->id){
            $route = route('websites.zones.update' , [$selected_website->id , $zone->id]) ;
            $method='PUT';
            $title = 'Edit';
        } else {
            $route = route('websites.zones.store' , [$selected_website->id]);
            $method='POST';
            $title = 'New';
        }
    @endphp

    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        {{ $title }} Zone
                    </h2>

                </div>
                <div class="panel-container show">
                    <div class="panel-content">

                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item" aria-selected="true">
                                <a class="nav-link active" data-toggle="tab" href="#tab_basic" role="tab" aria-selected="true">Basic</a>
                            </li>
                            @if ($zone->id)
                            <li class="nav-item" aria-selected="true">
                                <a class="nav-link" data-toggle="tab" href="#tab_banners" role="tab" aria-selected="false">Banners</a>
                            </li>
                            @endif
                        </ul>

                        <div class="tab-content p-3">
                            <div class="tab-pane fade active show" id="tab_basic" role="tabpanel" >
                                <form method="POST" action="{{ $route }}">
                                    @csrf
                                    @method($method)
                                    <input type="hidden" name="website_id" value="{{ $selected_website->id }}"/>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="zone-name">Name*</label>
                                        <div class="col-sm-10">
                                            <input name="name" type="text" class="form-control" id="zone-name" required
                                                   value="{{ old('name') ?? $zone->name }}"
                                            >
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="zone-rectangle_id">Rectangle*</label>
                                        <div class="col-sm-10">
                                            <select name="rectangle_id" class="form-control" id="zone-rectangle_id" required>
                                                <option value="">-----</option>
                                                @foreach (\App\Models\Rectangle::all() as $rectangle)
                                                    <option value="{{ $rectangle->id }}" {{ $zone->rectangle_id == $rectangle->id ? 'selected' : '' }}>
                                                        {{ $rectangle->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="zone-description">Description</label>
                                        <div class="col-sm-10">
                                            <input name="description" type="text" class="form-control" id="zone-description"
                                                   value="{{ old('description') ?? $zone->description }}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 col-form-label" for="zone-comments">Comments</label>
                                        <div class="col-sm-10">
                                    <textarea name="comments" class="form-control" id="zone-comments"
                                              rows="3">{{ old('comments') ?? $zone->comments }}</textarea>
                                        </div>
                                    </div>
                                    <div
                                        class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row-reverse">
                                        <button type="submit" class="btn btn-primary ml-3">Save changes</button>
                                        <a type="button" class="btn btn-secondary ml-3" href="javascript:javascript:history.go(-1)">Close</a>
                                    </div>
                                </form>
                            </div>

                            @if ($zone->id)
                            <div class="tab-pane fade" id="tab_banners" role="tabpanel" >
                                <ul>
                                    @foreach ($zone->banners as $banner)
                                        <li><a href="{{ route('banners.show' , [$banner->id]) }}">{{ $banner->name }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
