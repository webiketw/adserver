<div class="form-group row">
    <label class="col-sm-2 col-form-label" for="banner-status">Delivery</label>
    <div class="col-sm-10">
        <select class="form-control" id="banner-status" required name="status">
            <option value="">-----</option>
            <option value="1">Enable</option>
            <option value="0">Disable</option>
        </select>
    </div>
</div>



<div class="form-group row" id="js-page-content-demopanels">
    <label class="col-sm-2 col-form-label" for="banner-status">Logic</label>
    <div class="col-sm-10 card-body" >
        <div class="row">
            <div class="col-md-12">

                <div class="panel" style="flex-direction: row !important;"
                     data-panel-collapsed data-panel-fullscreen data-panel-close >

                    <div class="panel-hdr">
                        <h2>
                            <a>
                            <i class="fal fa-ellipsis-v mr-2"></i>
                            </a>
                        </h2>

                    </div>
                    <div class="panel-container show">
                        <h2 class="row ml-2 mb-0 align-items-center" style="height:80px;">
                            <div class="col">
                                <select class="form-control">
                                    <option>AND</option>
                                    <option>OR</option>
                                </select>
                            </div>

                            <div class="col">
                                <select class="form-control">
                                    <option>Page</option>
                                    <option>Role</option>
                                    <option>Category</option>
                                    <option>Motor</option>
                                    <option>Brand</option>
                                </select>
                            </div>


                            <div class="col">
                                <select class="form-control">
                                    <option>Equal</option>
                                    <option>Not Equal</option>
                                    <option>Less</option>
                                    <option>Less or Equal</option>
                                    <option>Great</option>
                                    <option>Great or Equal</option>
                                </select>
                            </div>



                            <div class="col">
                                <input class="form-control" type="text" />
                            </div>

                        </h2>
                    </div>
                </div>




                <div class="panel" style="flex-direction: row !important;"
                     data-panel-collapsed data-panel-fullscreen data-panel-close >

                    <div class="panel-hdr">
                        <h2>
                            <a>
                                <i class="fal fa-ellipsis-v mr-2"></i>
                            </a>
                        </h2>

                    </div>
                    <div class="panel-container show">
                        <h2 class="row ml-2 mb-0 align-items-center" style="height:80px;">
                            <div class="col">
                                <select class="form-control">
                                    <option>AND</option>
                                    <option>OR</option>
                                </select>
                            </div>

                            <div class="col">
                                <select class="form-control">
                                    <option>Page</option>
                                    <option>Role</option>
                                    <option>Category</option>
                                    <option>Motor</option>
                                    <option>Brand</option>
                                </select>
                            </div>


                            <div class="col">
                                <select class="form-control">
                                    <option>Equal</option>
                                    <option>Not Equal</option>
                                    <option>Less</option>
                                    <option>Less or Equal</option>
                                    <option>Great</option>
                                    <option>Great or Equal</option>
                                </select>
                            </div>



                            <div class="col">
                                <input class="form-control" type="text" />
                            </div>

                        </h2>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>


@push('page-scripts')

<script>

    $('#js-page-content-demopanels').smartPanel(
        {
            localStorage: false,
            onChange: function() {},
            onSave: function() {},
            opacity: 1,
            deleteSettingsKey: '#deletesettingskey-options',
            settingsKeyLabel: 'Reset settings?',
            deletePositionKey: '#deletepositionkey-options',
            positionKeyLabel: 'Reset position?',
            sortable: true,
            buttonOrder: '%collapse% %fullscreen% %close%',
            buttonOrderDropdown: '%refresh% %locked% %color% %custom% %reset%',
            customButton: false,
            closeButton: true,
            onClosepanel: function()
            {
                if (myapp_config.debugState)
                    console.log($(this).closest(".panel").attr('id') + " onClosepanel")
            },
            fullscreenButton: true,
            onFullscreen: function()
            {
                if (myapp_config.debugState)
                    console.log($(this).closest(".panel").attr('id') + " onFullscreen")
            },
            collapseButton: true,
            onCollapse: function()
            {
                if (myapp_config.debugState)
                    console.log($(this).closest(".panel").attr('id') + " onCollapse")
            },
            lockedButton: false,

            refreshButton: false,

            colorButton: false,

            resetButton: false
        });
</script>
@endpush
