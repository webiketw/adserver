<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rectangle extends Model
{
    use HasFactory;

    protected $fillable = ['width','height','type','comments'];

    public function zones()
    {
        return $this->hasMany(Zone::class);
    }

    public function getNameAttribute(){
        $formatter = '[%s] %s x %s';
        return sprintf($formatter , strtoupper($this->type) , $this->width , $this->height) ;
    }
}
