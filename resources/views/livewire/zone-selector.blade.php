<div class="col-sm-10">
    <select class="form-control" id="banner-zone_id" required
            name="zone_id" wire:model="zoneId">
        <option value="">-----</option>
        @foreach($zones as $zone)
            <option value="{{ $zone->id }}">{{ $zone->name }}</option>
        @endforeach
    </select>

    <span class="help-block">{{ $this->zone ? $this->zone->rectangle->name : ''}}</span>
</div>
