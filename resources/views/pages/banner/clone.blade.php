@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        New Banner
                    </h2>
                    <div class="btn-group">
                        <button class="btn btn-secondary btn-xs" type="button">
                            Advertiser : {{ $selected_advertiser->name}}
                        </button>
                    </div>
                    <div class="btn-group ml-3">
                        <button class="btn btn-secondary btn-xs" type="button">
                            Campaign : {{ $selected_campaign->name }}
                        </button>
                    </div>
                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <form method="POST" action="{{ route('advertisers.campaigns.banners.store' , [$selected_advertiser->id , $selected_campaign->id ]) }}"
                              enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="campaign_id" value="{{ $selected_campaign->id }}">

                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#tab_basic" role="tab" aria-selected="true">Basic</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tab_acl" role="tab" aria-selected="false">Acl</a>
                                </li>

                            </ul>

                            <div class="tab-content p-3">
                                <div class="tab-pane fade active show" id="tab_basic" role="tabpanel" >
                                    @include('pages.banner.partials.update-basic')
                                </div>

                                <div class="tab-pane fade" id="tab_acl" role="tabpanel" >
                                    @include('pages.banner.partials.update-acl')
                                </div>
                            </div>


                            <div
                                class="panel-content border-faded border-left-0 border-right-0 border-bottom-0 d-flex flex-row-reverse">
                                <button type="submit" class="btn btn-primary ml-3">Save changes</button>
                                <a type="button" class="btn btn-secondary ml-3" href="javascript:javascript:history.go(-1)">Close</a>
                            </div>




                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')


@endpush

