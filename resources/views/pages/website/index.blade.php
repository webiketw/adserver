@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-xl-12">
            <div id="panel-1" class="panel">
                <div class="panel-hdr">
                    <h2>
                        Website
                    </h2>

                    <div class="panel-toolbar">
                        <a href="{{ route('websites.create') }}" type="button" class="btn btn-xs btn-success ml-3">Create new website</a>
                    </div>

                </div>
                <div class="panel-container show">
                    <div class="panel-content">
                        <table class="table table-bordered table-hover table-striped">
                            <thead class="bg-fusion-50">
                                <th>
                                    Name
                                </th>
                                <th >
                                    Url
                                </th>

                                <th>
                                    Pages
                                </th>
                                <th>
                                    Zones
                                </th>

                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                            @foreach ($collection as $entity)
                                <tr>
                                    <td>
                                        {{ $entity->name }}
                                    </td>
                                    <td>
                                        <a href="{{ $entity->url }}" target="_blank">{{ $entity->url }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('websites.pages.index' , [$entity->id]) }}">{{ $entity->pages_count }}</a>
                                    </td>

                                    <td>
                                        <a href="{{ route('websites.zones.index' , [$entity->id]) }}">{{ $entity->zones_count }}</a>
                                    </td>

                                    <td class="text-right">
                                        <a type="button" class="btn btn-info btn-xs waves-effect waves-themed ml-2"
                                           href="{{ route('websites.pages.index' , [ $entity->id ]) }}">Pages</a>

                                        <a type="button" class="btn btn-info btn-xs waves-effect waves-themed ml-2"
                                           href="{{ route('websites.zones.index' , [ $entity->id ]) }}">Zones</a>

                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@push('page-scripts')
    <script type="text/javascript">


    </script>

@endpush
