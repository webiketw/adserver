<div class="col-sm-10">

    <input type="hidden" name="media-type" value="{{ $this->rectangle ? $this->rectangle->type : '' }}" required />

    @if ($this->rectangle)
        @switch($this->rectangle->type)
            {{--
            @case(App\Constants\RectangleType::IMAGE)
            <input class="form-control" type="file" name="media" {{ $this->media ? '' : 'required' }}/>
            <a href="{{ Illuminate\Support\Facades\Storage::disk('public')->url($this->media) }}" target="_blank"><span class="help-block">{{ $this->media }}</span></a>
            @break
            --}}

            @case(App\Constants\RectangleType::URL)
            <input class="form-control" type="url" name="media" required value="{{ $this->media }}"/>
            @break

            @case(App\Constants\RectangleType::HTML)
            <textarea class="form-control" name="media" row="5" required>{{ $this->media }}</textarea>
            @break

            @case(App\Constants\RectangleType::TEXT)
            <input class="form-control" type="text" name="media" required value="{{ $this->media }}"/>
            @break

        @endswitch
    @else

    @endif
</div>
